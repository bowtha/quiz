import React, { useState, useEffect } from 'react';
import { List, Descriptions, Icon, Select, Row, Col, Slider, PageHeader, BackTop } from 'antd';
import 'antd/dist/antd.css';
import '../todo.css';
const { Option } = Select;



const AlbumPage = (props) => {

    const [user, setUser] = useState({});
    const [albumList, setAlbumList] = useState([]);


    const fetchUserData = () => {
        const userID = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/users/' + userID)
            .then(response => response.json())
            .then(data => {
                setUser(data)
            })
    }

    const fetchAlbumData = () => {
        const userID = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/albums?userId=' + userID)
            .then(response => response.json())
            .then(album => {
                setAlbumList(album)
            })
    }

    useEffect(() => {
        fetchUserData();
    }, [])

    useEffect(() => {
        fetchAlbumData();
    }, [])

 
    return (
        <div>
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                }}
                onBack={() => { window.location = "/users" }}
                title="Back"
            />

            <Descriptions title="User Information" bordered>
                <Descriptions.Item label="Name">{user.name}</Descriptions.Item>
                <Descriptions.Item label="Username">{user.username}</Descriptions.Item>
                <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                <Descriptions.Item label="Phone">{user.phone}</Descriptions.Item>
                <Descriptions.Item label="Website">{user.website}</Descriptions.Item>
            </Descriptions>

            <div className="demo-infinite-container">

                <List
                    dataSource={albumList}
                    renderItem={(item, index) => (
                        <List.Item key={item.id}>

                            <List.Item.Meta
                                title={<a href={"/listAlbums/"+ item.id } ><Icon type="right" />{item.title}</a>}
                            />
                        </List.Item>
                    )}
                >

                </List>

            </div>
            <div>
                <BackTop>
                    <div className="ant-back-top-inner">UP</div>
                </BackTop>


            </div>
        </div>
    )
}

export default AlbumPage;