import React, { useState, useEffect } from 'react';
import { List, Descriptions, Icon, Select, Button, PageHeader, BackTop } from 'antd';
import 'antd/dist/antd.css';
import '../todo.css';
const { Option } = Select;



const TodoPage = (props) => {

    const [selectedFilter, setSelectedFilter] = useState(-1);
    const [user, setUser] = useState({});
    const [todoList, setTodolist] = useState([]);


    const handleChange = (value) => {
        console.log(`selected ${value}`);
        setSelectedFilter(value)
        //console.log(selectedFilter)
    }

    const fetchUserData = () => {
        const userID = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/users/' + userID)
            .then(response => response.json())
            .then(data => {
                setUser(data)
            })
    }

    const fetchTodoData = () => {
        const userID = props.match.params.user_id;
        fetch('https://jsonplaceholder.typicode.com/todos?userId=' + userID)
            .then(response => response.json())
            .then(todo => {
                setTodolist(todo)
            })
    }

    useEffect(() => {
        fetchUserData();
    }, [])

    useEffect(() => {
        fetchTodoData();
    }, [])

    const changeStatusTodo = (index) => {
        let newList = [...todoList]
        newList[index].completed = true
        setTodolist(newList)
        // console.log(newList)
    }

    const getStatustoShow = () => {
        console.log(selectedFilter)
        if (selectedFilter == -1) {
            return todoList
        }
        else {
            return todoList.filter(status => status.completed == selectedFilter)
        }
    }

    console.log(user)


    return (
        <div>
            <PageHeader
                style={{
                    border: '1px solid rgb(235, 237, 240)',
                }}
                onBack={() => { window.location = "/users" }}
                title="Back"
            />

            <Descriptions title="User Information" bordered>
                <Descriptions.Item label="Name">{user.name}</Descriptions.Item>
                <Descriptions.Item label="Username">{user.username}</Descriptions.Item>
                <Descriptions.Item label="Email">{user.email}</Descriptions.Item>
                <Descriptions.Item label="Phone">{user.phone}</Descriptions.Item>
                <Descriptions.Item label="Website">{user.website}</Descriptions.Item>
            </Descriptions>

            <div className="demo-infinite-container">

                <Select defaultValue="Status" style={{ width: 120 }} onChange={handleChange}>
                    <Option value={-1}>All</Option>
                    <Option value={true}>Done</Option>
                    <Option value={false}>Doing</Option>
                </Select>

                <List
                    dataSource={getStatustoShow()}
                    renderItem={(item, index) => (
                        <List.Item key={item.id}>

                            <List.Item.Meta
                                title={<a ><Icon type="right" />     {item.id}</a>}
                                description={item.title}
                            />
                            {
                                item.completed ?
                                    <Button type="dashed">
                                        Done
                                    </Button>
                                    :
                                    <Button type="primary" onClick={() => changeStatusTodo(index)}>
                                        Doing
                                    </Button>
                            }
                        </List.Item>
                    )}
                >

                </List>

            </div>
            <div>
                <BackTop>
                    <div className="ant-back-top-inner">UP</div>
                </BackTop>


            </div>
        </div>
    )
}

export default TodoPage;