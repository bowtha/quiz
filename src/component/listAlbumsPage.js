import React, { useState, useEffect } from 'react';
import { Card, Row, Col, PageHeader, BackTop } from 'antd';
import "../listAlbums.css"

const ListAlbums = (props) => {

  const [photoList, setPhotoList] = useState([]);

  const fetchListAlbum = () => {
    const albumID = props.match.params.album_id;
    fetch('https://jsonplaceholder.typicode.com/photos?albumId=' + albumID)
      .then(response => response.json())
      .then(photo => {
        setPhotoList(photo)
      })
  }

  useEffect(() => {
    fetchListAlbum();
  }, [])

  //console.log(props)

  return (
    <div>
      <PageHeader
        style={{
          border: '1px solid rgb(235, 237, 240)',
        }}
        onBack={() => {  window.history.back() }}
        title="Back"
      />

      <div>
        <div style={{ background: '#ECECEC', padding: '30px', marginTop: '10%' }}>
          <Row gutter={16}>
            {
              photoList.map(Item =>
                <Col span={8}>
                  <Card title={Item.title} bordered={false}>
                    <img src={Item.url} />
                  </Card>
                </Col>
              )
            }
          </Row>
        </div>
      </div>
      <div>
        <BackTop>
          <div className="ant-back-top-inner">UP</div>
        </BackTop>


      </div>
    </div>
  )
}

export default ListAlbums; 