import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import UserPage from './userpage'
import TodoPage from './todopage'
import AlbumPage from './albumpage'
import ListAlbums from './listAlbumsPage'

const Main = () =>{
    return(
        <BrowserRouter>
            <Route path="/" component={UserPage} exact={true}></Route>
            <Route path="/users" component={UserPage} exact={true}></Route>
            <Route path="/users/:user_id/todo" component={TodoPage}></Route>
            <Route path="/users/:user_id/album" component={AlbumPage}></Route>
            <Route path="/listAlbums/:album_id" component={ListAlbums}></Route>
        </BrowserRouter>
            

    )
}  
export default Main;