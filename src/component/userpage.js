import React, { useEffect, useState } from 'react';
import { List, Avatar, Typography, Skeleton, Input,Icon } from 'antd';
import 'antd/dist/antd.css';
import '../userpage.css';


const { Search } = Input;
const { Title } = Typography;

const UserPage = () => {

    const [users, setUsers] = useState([])
    const [name, setName] = useState()


    const fetchUserData = () => {
        if (name) {
            //console.log(name)
            let newList = users.filter(data => (data.name.match(name)));
            setUsers(newList)
            //console.log("result", newList)
        }
        else {
            fetch('https://jsonplaceholder.typicode.com/users')
                .then(response => response.json())
                .then(data => {
                    setUsers(data)
            })
        }
    }

    useEffect(() => {
        fetchUserData();
    }, [name])


    const onSearch = e => {
        setName(e.target.value);


    }

    return (
        <div>
            <Title>All USERS</Title>
            <Search
                placeholder="input search name"
                onChange={onSearch}
                style={{ width: 200 }}
            />
            <List
                className="demo-loadmore-list"
                itemLayout="horizontal"
                dataSource={users}
                renderItem={item => (
                    <List.Item
                        actions={[<a href={"/users/" + item.id + "/todo"}><Icon type="unordered-list" /></a>,
                        <a href={"/users/" + item.id + "/album"}><Icon type="area-chart" /></a>
                    ]}
                    >
                        <Skeleton avatar title={false} loading={item.loading} active>
                            <List.Item.Meta
                                avatar={
                                    <Avatar style={{ backgroundColor: '#87d068' }} icon="user" />
                                }
                                title={<a href="https://ant.design">{item.name}</a>}
                                description={item.email}
                            />
                        </Skeleton>
                    </List.Item>
                )}
            />
        </div>
    );
}



export default UserPage;